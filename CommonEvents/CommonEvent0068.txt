// EVENT 0068
Name "Skill: Play 281-290"

ChangeVariable(11,11,0,2,1,5)
If(1,100,0,281,0)
 If(1,11,0,1,0)
  ShowMessageFace("succubuses_fc1",0,0,2,1)
  ShowMessage("【ハーレム】")
  ShowMessage("それで、その娘がね……")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("ハーレムはトークに花を咲かせている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("succubuses_fc1",0,0,2,3)
  ShowMessage("【ハーレム】")
  ShowMessage("うふふっ……♪")
  355("interrupt_skill(3296)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("succubuses_fc1",0,0,2,4)
  ShowMessage("【ハーレム】")
  ShowMessage("みんな、踊るわよ～♪")
  ShowMessageFace("",0,0,2,5)
  ShowMessage("ハーレムはゆかいなダンスを踊った！")
  ShowMessage("しかし何も起きなかった……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("succubuses_fc1",0,0,2,6)
  ShowMessage("【ハーレム】")
  ShowMessage("これ、サービスねっ♪")
  ShowMessageFace("",0,0,2,7)
  ShowMessage("ハーレムからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(342,0,0,1)
  ShowMessageFace("",0,0,2,8)
  ShowMessage("「ミルク」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("succubuses_fc1",0,0,2,9)
  ShowMessage("【ハーレム】")
  ShowMessage("うふふっ、魔眼だよ……♪")
  355("interrupt_skill(2561)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,282,0)
 If(1,11,0,1,0)
  ShowMessageFace("maycubbus_fc1",0,0,2,10)
  ShowMessage("【メイ】")
  ShowMessage("はい、がんばって下さい……")
  ShowMessageFace("",0,0,2,11)
  ShowMessage("メイはみんなを応援した！")
  ShowMessage("しかし誰も聞いていない……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("maycubbus_fc1",0,0,2,12)
  ShowMessage("【メイ】")
  ShowMessage("私がご主人様になりましょう。")
  ShowMessageFace("",0,0,2,13)
  ShowMessage("メイは大胆に宣言した！")
  ShowMessage("しかし誰も聞いていない……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("maycubbus_fc1",2,0,2,14)
  ShowMessage("【メイ】")
  ShowMessage("失礼、寝ます……")
  313(0,282,0,11)
  ShowMessageFace("",0,0,2,15)
  ShowMessage("メイは居眠りしている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("maycubbus_fc1",0,0,2,16)
  ShowMessage("【メイ】")
  ShowMessage("これをどうぞ……")
  ShowMessageFace("",0,0,2,17)
  ShowMessage("メイからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(375,0,0,1)
  ShowMessageFace("",0,0,2,18)
  ShowMessage("「目玉焼き」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("maycubbus_fc1",0,0,2,19)
  ShowMessage("【メイ】")
  ShowMessage("綺麗にしましょう……")
  355("interrupt_skill(2367)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,283,0)
 If(1,11,0,1,0)
  ShowMessageFace("maccubus_fc1",5,0,2,20)
  ShowMessage("【メリッサ】")
  ShowMessage("うぇ～ん！")
  ShowMessageFace("",0,0,2,21)
  ShowMessage("メリッサは嘘泣きをした！")
  ShowMessage("しかし、誰も聞いていない……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("maccubus_fc1",1,0,2,22)
  ShowMessage("【メリッサ】")
  ShowMessage("おめでとう！")
  ShowMessageFace("",0,0,2,23)
  ShowMessage("メリッサはクラッカーを鳴らした！")
  ShowMessage("しかし誰も相手にしない……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("maccubus_fc1",5,0,2,24)
  ShowMessage("【メリッサ】")
  ShowMessage("来ないで！")
  355("interrupt_skill(1437)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("maccubus_fc1",1,0,2,25)
  ShowMessage("【メリッサ】")
  ShowMessage("はい、どうぞ。")
  ShowMessageFace("",0,0,2,26)
  ShowMessage("メリッサからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(358,0,0,1)
  ShowMessageFace("",0,0,2,27)
  ShowMessage("「ハンバーガー」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("maccubus_fc1",1,0,2,28)
  ShowMessage("【メリッサ】")
  ShowMessage("うふふっ……")
  355("interrupt_skill(3296)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,284,0)
 If(1,11,0,1,0)
  ShowMessageFace("minccubus_fc1",2,0,2,29)
  ShowMessage("【セーラ】")
  ShowMessage("えへへっ……♪")
  ShowMessageFace("",0,0,2,30)
  ShowMessage("セーラは地面に落書きをしている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("minccubus_fc1",3,0,2,31)
  ShowMessage("【セーラ】")
  ShowMessage("ぎゃん！")
  355("interrupt_skill(3293)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("minccubus_fc1",2,0,2,32)
  ShowMessage("【セーラ】")
  ShowMessage("がんばれ～♪")
  355("interrupt_skill(3300)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("minccubus_fc1",2,0,2,33)
  ShowMessage("【セーラ】")
  ShowMessage("はい、飲んで～♪")
  ShowMessageFace("",0,0,2,34)
  ShowMessage("セーラからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(342,0,0,1)
  ShowMessageFace("",0,0,2,35)
  ShowMessage("「ミルク」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("minccubus_fc1",3,0,2,36)
  ShowMessage("【セーラ】")
  ShowMessage("やだっ！")
  355("interrupt_skill(1437)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,285,0)
 If(1,11,0,1,0)
  ShowMessageFace("renccubus_fc1",3,0,2,37)
  ShowMessage("【ニーナ】")
  ShowMessage("うふふっ……♪")
  355("interrupt_skill(3296)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("renccubus_fc1",2,0,2,38)
  ShowMessage("【ニーナ】")
  ShowMessage("あははっ……♪")
  ShowMessageFace("",0,0,2,39)
  ShowMessage("ニーナは陽気に歌って踊っている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("renccubus_fc1",2,0,2,40)
  ShowMessage("【ニーナ】")
  ShowMessage("えいっ！")
  355("interrupt_skill(3307)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("renccubus_fc1",2,0,2,41)
  ShowMessage("【ニーナ】")
  ShowMessage("うちの牧場で搾った新鮮ミルクですよ♪")
  ShowMessageFace("",0,0,2,42)
  ShowMessage("ニーナからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(342,0,0,1)
  ShowMessageFace("",0,0,2,43)
  ShowMessage("「ミルク」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("renccubus_fc1",4,0,2,44)
  ShowMessage("【ニーナ】")
  ShowMessage("変態！")
  355("interrupt_skill(1437)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,286,0)
 If(1,11,0,1,0)
  ShowMessageFace("witchs_fc1",1,0,2,45)
  ShowMessage("【ウィッチ】")
  ShowMessage("うふふっ……")
  355("interrupt_skill(3296)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("witchs_fc1",0,0,2,46)
  ShowMessage("【ウィッチ】")
  ShowMessage("不思議なタネで……えいっ！")
  355("interrupt_skill(3299)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("witchs_fc1",1,0,2,47)
  ShowMessage("【ウィッチ】")
  ShowMessage("たまにはハメを外そうかしら……♪")
  ShowMessageFace("",0,0,2,48)
  ShowMessage("ウィッチは陽気に歌って踊っている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("witchs_fc1",0,0,2,49)
  ShowMessage("【ウィッチ】")
  ShowMessage("これを食べると、魔導が身につく……かも？")
  ShowMessageFace("",0,0,2,50)
  ShowMessage("ウィッチからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(304,0,0,1)
  ShowMessageFace("",0,0,2,51)
  ShowMessage("「さくらんぼ」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("witchs_fc1",0,0,2,52)
  ShowMessage("【ウィッチ】")
  ShowMessage("新しい魔法を考案したわ……")
  ShowMessage("ウルトラファイア！")
  355("interrupt_skill(3306)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,287,0)
 If(1,11,0,1,0)
  ShowMessageFace("succubus_fc1",1,0,2,53)
  ShowMessage("【サキュバス】")
  ShowMessage("ニコッ♪")
  ShowMessageFace("",0,0,2,54)
  ShowMessage("サキュバスはにっこり微笑んだ！")
  ShowMessage("しかし何も起きなかった……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("succubus_fc1",0,0,2,55)
  ShowMessage("【サキュバス】")
  ShowMessage("たまには、こういう遊びもね……")
  355("interrupt_skill(3295)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("succubus_fc1",0,0,2,56)
  ShowMessage("【サキュバス】")
  ShowMessage("私の目を見なさい……")
  355("interrupt_skill(2561)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("succubus_fc1",0,0,2,57)
  ShowMessage("【サキュバス】")
  ShowMessage("はい、差し入れよ。")
  ShowMessageFace("",0,0,2,58)
  ShowMessage("サキュバスからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(319,0,0,3)
  ShowMessageFace("",0,0,2,59)
  ShowMessage("「じゃがいも」を3つ手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("succubus_fc1",0,0,2,60)
  ShowMessage("【サキュバス】")
  ShowMessage("ふふっ……")
  355("interrupt_skill(3296)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,288,0)
 If(1,11,0,1,0)
  ShowMessageFace("d_succubus_fc1",0,0,2,61)
  ShowMessage("【ナターシャ】")
  ShowMessage("踊りますよ～♪")
  ShowMessageFace("",0,0,2,62)
  ShowMessage("ナターシャはゆかいなダンスを踊った！")
  ShowMessage("しかし何も起きなかった……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("d_succubus_fc1",0,0,2,63)
  ShowMessage("【ナターシャ】")
  ShowMessage("ふふふふっ……")
  355("interrupt_skill(3297)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("d_succubus_fc1",0,0,2,64)
  ShowMessage("【ナターシャ】")
  ShowMessage("こういうのも、楽しいでしょう……？")
  355("interrupt_skill(2561)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("d_succubus_fc1",0,0,2,65)
  ShowMessage("【ナターシャ】")
  ShowMessage("これ、あげます。")
  ShowMessage("出所は聞かないで下さいね……")
  ShowMessageFace("",0,0,2,66)
  ShowMessage("ナターシャからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(319,0,0,1)
  ShowMessageFace("",0,0,2,67)
  ShowMessage("「じゃがいも」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("d_succubus_fc1",0,0,2,68)
  ShowMessage("【ナターシャ】")
  ShowMessage("ふふふふっ……")
  355("interrupt_skill(3298)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,289,0)
 If(1,11,0,1,0)
  ShowMessageFace("maidscyulla_fc1",1,0,2,69)
  ShowMessage("【ラン】")
  ShowMessage("触手で失礼……")
  355("interrupt_skill(2776)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("maidscyulla_fc1",1,0,2,70)
  ShowMessage("【ラン】")
  ShowMessage("私の踊りを楽しんでもらいましょう……♪")
  ShowMessageFace("",0,0,2,71)
  ShowMessage("ランはゆかいなダンスを踊った！")
  ShowMessage("しかし何も起きなかった……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("maidscyulla_fc1",1,0,2,72)
  ShowMessage("【ラン】")
  ShowMessage("メイドたる者、身だしなみが大事……")
  ShowMessageFace("",0,0,2,73)
  ShowMessage("ランは身だしなみを整えている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("maidscyulla_fc1",1,0,2,74)
  ShowMessage("【ラン】")
  ShowMessage("私の料理をご賞味下さい……")
  ShowMessageFace("",0,0,2,75)
  ShowMessage("ランからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(364,0,0,1)
  ShowMessageFace("",0,0,2,76)
  ShowMessage("「オムライス」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("maidscyulla_fc1",1,0,2,77)
  ShowMessage("【ラン】")
  ShowMessage("ご主人様のために……")
  355("interrupt_skill(3294)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,290,0)
 If(1,11,0,1,0)
  ShowMessageFace("madaminsect_fc1",0,0,2,78)
  ShowMessage("【シルヴィア】")
  ShowMessage("このパラソルは素敵でしょう……")
  ShowMessageFace("",0,0,2,79)
  ShowMessage("シルヴィアは傘をクルクル回している……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("madaminsect_fc1",0,0,2,80)
  ShowMessage("【シルヴィア】")
  ShowMessage("私が貴婦人の村の長となりましょう！")
  ShowMessageFace("",0,0,2,81)
  ShowMessage("シルヴィアは大胆に宣言した！")
  ShowMessage("しかし誰も聞いていない……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("madaminsect_fc1",0,0,2,82)
  ShowMessage("【シルヴィア】")
  ShowMessage("いい天気ですわね……")
  ShowMessageFace("",0,0,2,83)
  ShowMessage("シルヴィアはぼんやりしている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("madaminsect_fc1",0,0,2,84)
  ShowMessage("【シルヴィア】")
  ShowMessage("これをどうぞ……")
  ShowMessageFace("",0,0,2,85)
  ShowMessage("シルヴィアからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(345,0,0,1)
  ShowMessageFace("",0,0,2,86)
  ShowMessage("「クロワッサン」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("madaminsect_fc1",0,0,2,87)
  ShowMessage("【シルヴィア】")
  ShowMessage("火遊びは好きですのよ……")
  355("interrupt_skill(3295)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
0()
